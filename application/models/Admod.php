<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admod extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	function cek_login($table,$where)
	{	
		return $this->db->get_where($table,$where);
	}	
	function tampil_students()
	{
		return $this->db->get('students');
	}
	function jumlahsiswa()
	{
		$query = $this->db->get('students');
		if ($query->num_rows()>0) {
			return $query->num_rows();;
		}else{
			return 0;
		}
	}
	function jumlahguru()
	{
		$query = $this->db->get('teacher');
		if ($query->num_rows()>0) {
			return $query->num_rows();;
		}else{
			return 0;
		}
	}
	function jumlahexams()
	{
		$query = $this->db->get('tasks');
		if ($query->num_rows()>0) {
			return $query->num_rows();;
		}else{
			return 0;
		}
	}
	function jumlahsekolah()
	{
		$query = $this->db->get('sekolah');
		if ($query->num_rows()>0) {
			return $query->num_rows();;
		}else{
			return 0;
		}
	}
	function tampiluser($username)
	{
		$this->db->where('username',$username);
		return $this->db->get('admin');
	}
	function pengumuman()
	{
		return $this->db->get('pengumuman');
	}
	function input_data($data,$table)
	{
		$this->db->insert($table,$data);
	}
	function hapus_data($where,$table)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}
	function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
	function tampil_exams()
	{
		return $this->db->get('tasks');
	}
	function tampil_teacher()
	{
		return $this->db->get('teacher');
	}
}

/* End of file admod.php */
/* Location: ./application/models/admod.php */