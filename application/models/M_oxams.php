<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_oxams extends CI_Model {

	function tampil_tasks()
	{
		return $this->db->get('tasks');
	}	
	function tampiluser($username)
	{
		$this->db->where('username',$username);
		return $this->db->get('students');
	}
	public function get_soal()
	{
		$this->db->select('*');
		$this->db->from('soal');
		$this->db->join('tasks', 'tasks.id_tasks = soal.id_task');
		$query = $this->db->get();
		return $query->result();
	}
	public function soal($where,$table)
	{
		return $this->db->get_where($table,$where);
	}
	function input_jawaban($data,$table)
	{
		$this->db->insert($table,$data);
	}
}

/* End of file M_oxams.php */
/* Location: ./application/models/M_oxams.php */