<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Oxams extends CI_Controller {

	public function index()
	{
		$this->load->view('oxams');		
	}
	public function dashboard()
	{
		$this->load->model('m_login');
		$this->load->model('m_oxams');
		$this->load->model('admod');
		// $data['students'] = $this->m_login->tampil_students($this->session->userdata('username'));
		$data['tasks'] = $this->m_oxams->tampil_tasks()->result();
		$data['pengumuman'] = $this->admod->pengumuman()->result();
		$data['user'] = $this->m_oxams->tampiluser($this->session->userdata('username'))->result();
		$this->load->view('dashboard',$data);
	}
	public function profile()
	{
		$this->load->model('m_oxams');
		$data['user'] = $this->m_oxams->tampiluser($this->session->userdata('username'))->result();
		$this->load->view('profile',$data);
	}
	public function soal($id_tasks)
	{
		$this->load->model('m_oxams');
		$where = array('id_tasks' => $id_tasks);
		$data['soal'] = $this->m_oxams->soal($where,'tasks')->result();
		$data['pertanyaan'] = $this->m_oxams->get_soal();
		$data['user'] = $this->m_oxams->tampiluser($this->session->userdata('username'))->result();
		$this->load->view('soal', $data);
	}
	public function answer()
	{
		$this->load->model('m_oxams');
		$id_task = $this->input->post('id_task');
		$penjawab = $this->input->post('penjawab');
		$answer1 = $this->input->post('answer1');
		$answer2 = $this->input->post('answer2');
		$answer3 = $this->input->post('answer3');
		$answer4 = $this->input->post('answer4');
		$answer5 = $this->input->post('answer5');
		$answer6 = $this->input->post('answer6');
		$answer7 = $this->input->post('answer7');
		$answer8 = $this->input->post('answer8');
		$answer9 = $this->input->post('answer9');
		$answer10 = $this->input->post('answer10');
		$data = array(
			'id_task' => $id_task,
			'penjawab' => $penjawab,
			'answer1' => $answer1,
			'answer2' => $answer2,
			'answer3' => $answer3,
			'answer4' => $answer4,
			'answer5' => $answer5,
			'answer6' => $answer6,
			'answer7' => $answer7,
			'answer8' => $answer8,
			'answer9' => $answer9,
			'answer10' => $answer10 
			);
		$this->m_oxams->input_jawaban($data, 'answer');
		redirect(base_url('index.php/oxams/dashboard'));
	}
}

/* End of file Oxams.php */
/* Location: ./application/controllers/Oxams.php */