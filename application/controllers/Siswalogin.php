<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswalogin extends CI_Controller {
	
	public function login()
	{
		$this->load->view('login');
	}
	public function aksilogin()
	{
		$this->load->model('m_login');
		$nm_stud = $this->input->post('nm_stud');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => md5($password)
			);
		$cek = $this->m_login->cek_login("students",$where)->num_rows();
		if($cek > 0){

			$data_session = array(
				'username' => $username,
				'status' => "login"
				);
 
 			$this->session->set_userdata('logged_in', TRUE);
			$this->session->set_userdata($data_session);
 
			redirect(base_url("index.php/oxams/dashboard"));
 
		}else{
			echo "Username dan password salah !";
		}
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url("index.php/Siswalogin/login"));
	}
}

/* End of file Siswalogin.php */
/* Location: ./application/controllers/Siswalogin.php */