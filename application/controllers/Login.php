<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		$this->load->view('admin/login');	
	}
	public function aksi_login()
	{
		$this->load->model('admod');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => md5($password)
			);
		$cek = $this->admod->cek_login("admin",$where)->num_rows();
		if($cek > 0){
 
			$data_session = array(
				'username' => $username,
				'password' => md5($password)
				);
 
			$this->session->set_userdata('admin',TRUE);
			$this->session->set_userdata($data_session);
 
			redirect(base_url("index.php/admin/dashboard"));
 
		}else{
			echo "You're Not Admin !";
		}
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url('index.php/login'));
	}

}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */