<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('admin') != TRUE)
        { 
            redirect('login');
        }
	}
	public function tambah()
	{
		$this->load->model('admod');
		$nis = $this->input->post('nis');
		$nm_stud = $this->input->post('nm_stud');
		$asal_sekolah = $this->input->post('asal_sekolah');
		$kelas = $this->input->post('kelas');
		$jurusan = $this->input->post('jurusan');
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));
 
		$data = array(
			'nis' => $nis,
			'nm_stud' => $nm_stud,
			'asal_sekolah' => $asal_sekolah,
			'kelas' => $kelas,
			'jurusan' => $jurusan,
			'username' => $username,
			'password' => $password
			);
		$this->admod->input_data($data,'students');
		redirect(base_url('index.php/admin/dashboard'));
	}
	public function newPengumuman()
	{
		$this->load->model('admod');
		$author = $this->input->post('author');
		$pengumuman = $this->input->post('pengumuman');
		$created_at = $this->input->post('created_at');
		$data = array(
			'author' => $author,
			'pengumuman' => $pengumuman,
			'created_at' => $created_at 
		);
		$this->admod->input_data($data, 'pengumuman');
		redirect(base_url('index.php/admin/dashboard'));
	}
	public function tambah_task()
	{
		$this->load->model('admod');
		$jenis = $this->input->post('jenis');
		$paket = $this->input->post('paket');
		$jumlah_soal = $this->input->post('jumlah_soal');
		$waktu = $this->input->post('waktu');

		$data = array(
			'jenis' => $jenis,
			'paket' => $paket,
			'jumlah_soal' => $jumlah_soal,
			'waktu' => $waktu
			);
		$this->admod->input_data($data,'tasks');
		redirect(base_url('index.php/admin/data_exams'));
	}
	public function add_teacher()
	{
		$this->load->model('admod');
		$nip = $this->input->post('nip');
		$nm_teach = $this->input->post('nm_teach');
		$sekolah = $this->input->post('sekolah');
		$password = md5($this->input->post('password'));

		$data = array(
			'nip' => $nip,
			'nm_teach' => $nm_teach,
			'sekolah' => $sekolah,
			'password' => $password
		);
		$this->admod->input_data($data,'teacher');
		redirect(base_url('index.php/admin/data_teacher'));
	}

	public function dashboard()
	{
		$this->load->model('admod');
		$data['pengumuman'] = $this->admod->pengumuman()->result();
		$data['total_siswa'] = $this->admod->jumlahsiswa();
		$data['total_exams'] = $this->admod->jumlahexams();
		$data['total_guru'] = $this->admod->jumlahguru();
		$data['total_sekolah'] = $this->admod->jumlahsekolah();
		$data['user'] = $this->admod->tampiluser($this->session->userdata('username'))->result();
		$this->load->view('admin/admin',$data);
	}
	public function data_siswa()
	{
		$this->load->model('admod');
		$data['students'] = $this->admod->tampil_students()->result();
		$data['user'] = $this->admod->tampiluser($this->session->userdata('username'))->result();
		$this->load->view('admin/siswa',$data);
	}
	public function data_teacher()
	{
		$this->load->model('admod');
		$data['teacher'] = $this->admod->tampil_teacher()->result();
		$data['user'] = $this->admod->tampiluser($this->session->userdata('username'))->result();
		$this->load->view('admin/teacher', $data);
	}
	public function data_exams()
	{
		$this->load->model('admod');
		$data['tasks'] = $this->admod->tampil_exams()->result();
		$data['user'] = $this->admod->tampiluser($this->session->userdata('username'))->result();
		$this->load->view('admin/exams', $data);
	}
	//Update one item
	public function update()
	{
		$this->load->model('admod');
		$id_stud = $this->input->post('id_stud');
		$nis = $this->input->post('nis');
		$nm_stud = $this->input->post('nm_stud');
		$asal_sekolah = $this->input->post('asal_sekolah');
		$kelas = $this->input->post('kelas');
		$jurusan = $this->input->post('jurusan');
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));
 
		$data = array(
			'nis' => $nis,
			'nm_stud' => $nm_stud,
			'asal_sekolah' => $asal_sekolah,
			'kelas' => $kelas,
			'jurusan' => $jurusan,
			'username' => $username,
			'password' => $password
		);
 
		$where = array(
			'id_stud' => $id_stud
		);
 
		$this->admod->update_data($where,$data,'students');
		redirect(base_url('index.php/admin/dashboard'));
	}

	//Delete one item
	public function delete($id)
	{
		$this->load->model('admod');
		$where = array('id_stud' => $id);
		$this->admod->hapus_data($where,'students');
		redirect('admin/dashboard');
	}
	public function delete_teach($id)
	{
		$this->load->model('admod');
		$where = array('id_teach' => $id);
		$this->admod->hapus_data($where,'teacher');
		redirect('admin/data_teacher');
	}
	public function delete_task($id)
	{
		$this->load->model('admod');
		$where = array('id_tasks' => $id);
		$this->admod->hapus_data($where,'tasks');
		redirect('admin/data_exams');
	}
}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */
