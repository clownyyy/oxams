<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin| OXAMS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?= base_url('assets')?>/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url('assets')?>/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= base_url('assets')?>/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('assets')?>/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?= base_url('assets')?>/dist/css/skins/_all-skins.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?=base_url('assets/')?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?= base_url('assets')?>/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?= base_url('assets')?>/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?= base_url('assets')?>/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?= base_url('assets')?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?= base_url('assets')?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <style>
    .btn-circle {
  width: 30px;
  height: 30px;
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.428571429;
  border-radius: 15px;
}
  </style>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<script>function myFunction(){swal("Login Success","Selamat Datang <?php echo($this->session->userdata('username')) ?>","success")}</script>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>XM</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>OXM</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?= base_url('assets/')?>dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php foreach ($user as $user) {echo $user->fullname;} ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?= base_url('assets')?>/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  <?=$user->fullname?>
                  <small>Administrator</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?=base_url('index.php/login/logout')?>" class="btn btn-block btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?=base_url('assets/')?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?=$user->fullname?></p>
          <a href="#"><i class="fa fa-user text-success"></i> Administrator</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="<?=base_url('index.php/admin/dashboard')?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
        </li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-database"></i> <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="<?=base_url('index.php/admin/data_teacher')?>"><i class="fa fa-user"></i> Guru</a></li>
            <li><a href="<?=base_url('index.php/admin/data_siswa')?>"><i class="fa fa-users"></i> Siswa</a></li>
            <li><a href="<?=base_url('index.php/admin/data_exams')?>"><i class="fa fa-book"></i> Exams</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <div class="content-wrapper">
  	<div class="content-header"><h3>Data Guru</h3></div>
  	<div class="content">
     <div class="row">
       <div class="col-md-12">
         <div class="box box-success">
           <div class="box-header">
            <div class="pull-right"><a class="btn btn-success" href="" data-toggle="modal" data-target="#TeachersAdd"><i class="fa fa-plus"></i> New Teacher</a></div>
           </div>
           <div class="box-body">
           	<table class="table table-bordered table-striped" id="example1">
           		<thead>
           			<tr>
           				<th>No</th>
           				<th>NIP</th>
           				<th>Nama Guru</th>
           				<th>Sekolah</th>
           				<th>Password</th>
                  <th>Aksi</th>
           			</tr>
           		</thead>
           		<?php
           		$no = 1;
           		foreach ($teacher as $teach) { ?>
           		<tbody>
           			<tr>
           				<td><?=$no++;?></td>
           				<td><?=$teach->NIP?></td>
           				<td><?=$teach->nm_teach?></td>
           				<td><?=$teach->sekolah?></td>
                  <td><?=$teach->password?></td>
           				<td><a class="btn btn-circle btn-warning" data-toggle="modal" data-target="#updateTeachers<?=$teach->id_teach?>"><i class="fa fa-pencil"></i></a> <a href="<?= base_url('index.php/admin/delete_teach/'.$teach->id_teach)?>" class="btn btn-danger btn-circle"><i class="fa fa-trash"></i></a></td>
           			</tr>
           		</tbody>
           	<?php } ?>
           	</table>
           </div>
         </div>
       </div>
     </div> 
    </div>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="http://smkn10jakarta.sch.id/" target="_blank">ESEMKATEN</a>.</strong> All rights
    reserved.
  </footer>
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<div id="TeachersAdd" role="dialog" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Guru</h4>
      </div>
      <form class="form-horizontal" action="<?=base_url('index.php/admin/add_teacher')?>" method="post">
        <div class="modal-body">
          <div class="form-group">
            <label class="control-label col-xs-3">NIP</label>
            <div class="col-xs-9"><input type="number" class="form-control" name="nip"></div>
          </div>
          <div class="form-group">
            <label class="control-label col-xs-3">Nama Guru</label>
            <div class="col-xs-9"><input type="text" class="form-control" name="nm_teach"></div>
          </div>
          <div class="form-group">
            <label class="control-label col-xs-3">Asal Sekolah</label>
            <div class="col-xs-9"><input type="text" class="form-control" name="sekolah"></div>
          </div>
          <div class="form-group">
            <label class="control-label col-xs-3">Password</label>
            <div class="col-xs-9"><input type="password" class="form-control" name="password"></div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
          <button class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php foreach ($teacher as $teach) {?>
<div id="updateTeachers<?=$teach->id_teach?>" role="dialog" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Guru</h4>
      </div>
      <form class="form-horizontal" action="<?=base_url('index.php/admin/add_teacher')?>" method="post">
        <div class="modal-body">
          <div class="form-group">
            <label class="control-label col-xs-3">NIP</label>
            <div class="col-xs-9"><input type="number" class="form-control" value="<?=$teach->NIP?>" name="nip"></div>
          </div>
          <div class="form-group">
            <label class="control-label col-xs-3">Nama Guru</label>
            <div class="col-xs-9"><input type="text" class="form-control" value="<?=$teach->nm_teach?>" name="nm_teach"></div>
          </div>
          <div class="form-group">
            <label class="control-label col-xs-3">Asal Sekolah</label>
            <div class="col-xs-9"><input type="text" class="form-control" value="<?=$teach->sekolah?>" name="sekolah"></div>
          </div>
          <div class="form-group">
            <label class="control-label col-xs-3">Password</label>
            <div class="col-xs-9"><input type="password" class="form-control" value="" name="password"></div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
          <button class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php } ?>
<!-- Sweetalert -->
<script src="<?=base_url('assets')?>/sweetalert.min.js"></script>
<!-- jQuery 3 -->
<script src="<?= base_url('assets')?>/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?= base_url('assets')?>/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url('assets')?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?=base_url('assets/')?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- Morris.js charts -->
<script src="<?= base_url('assets')?>/bower_components/raphael/raphael.min.js"></script>
<script src="<?= base_url('assets')?>/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?= base_url('assets')?>/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?= base_url('assets')?>/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?= base_url('assets')?>/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?= base_url('assets')?>/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?= base_url('assets')?>/bower_components/moment/min/moment.min.js"></script>
<script src="<?= base_url('assets')?>/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?= base_url('assets')?>/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?= base_url('assets')?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?= base_url('assets')?>/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url('assets')?>/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('assets')?>/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?= base_url('assets')?>/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url('assets')?>/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="<?=base_url('assets/')?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url('assets/')?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
  $(function () {
    $('#example1').DataTable()
  })
</script>
</body>
</html>
