<!DOCTYPE html>
<html>
<head>
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style type="text/css">
        .login-container{
    margin-top: 5%;
    margin-bottom: 5%;
}
.login-form-1{
    padding: 5%;
    box-shadow: 0 5px 8px 0 rgba(0, 0, 0, 0.2), 0 9px 26px 0 rgba(0, 0, 0, 0.19);
    background: white;
}
.login-form-1 h3{
    text-align: center;
    color: #333;
}
.login-form-2{
    padding: 5%;
    background: #0062cc;
    box-shadow: 0 5px 8px 0 rgba(0, 0, 0, 0.2), 0 9px 26px 0 rgba(0, 0, 0, 0.19);
}
.login-form-2 h3{
    text-align: center;
    color: #fff;
}
.login-container form{
    padding: 10%;
}
.btnSubmit
{
    width: 50%;
    border-radius: 1rem;
    padding: 1.5%;
    border: none;
    cursor: pointer;
}
.login-form-1 .btnSubmit{
    font-weight: 600;
    color: #fff;
    background-color: #0062cc;
}
body {
    background: #d2d6de;
}
    </style>
    <title>Login | OXAMS</title>
</head>
<body>
<div class="container login-container">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4 login-form-1">
            <a href="<?=base_url()?>" style="text-decoration: none;"><h3>Login For Exams</h3></a>
            <form action="<?=base_url('index.php/siswalogin/aksilogin')?>" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Username *" name="username" value="" />
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password *" name="password" value="" />
                </div>
                <div class="form-group">
                    <button class="btn btnSubmit">Login</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>